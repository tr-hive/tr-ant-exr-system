import mongo = require("da-mongo-rx");
import rabbit = require("da-rabbitmq-rx");
import logs = require("da-logs");
import Rx = require("rx");
import trAnt = require("tr-ant-utils"); 
import handler = require("./handler");

import tulpe = trAnt.tulpe;

const PKG_NAME = require("../package.json").name;

export interface IHandleNotifRequestsOpts {
	logger: logs.ILogger
	pub: rabbit.RabbitPub
	requestsStream : Rx.Observable<trAnt.INotifRequest<any>>
	portfColl: mongo.Collection
}

export function handleNotifRequests(opts: IHandleNotifRequestsOpts) {
	var portfsStream = opts.requestsStream.filter(val => val.type == "INotifPortfolioChanged")
	.do(val => opts.logger.write({oper: "req", status : "start", request : val}))
	.flatMap((val: trAnt.INotifRequest<trAnt.INotifRequestPortfolioQuery>) => 
		queryPortf(opts.portfColl, val.query.portfolio).map(portf => tulpe(val, portf)))				
	.do(tlp => {if (tlp.val2 == null) {
		opts.logger.write({oper: "req", status : "not_found", request : tlp.val1})
	}})
	//emit only when portf exists
	.filter(tlp => tlp.val2)		
	.map(tlp => tulpe(tlp.val1, handler.mapPortfolio(tlp.val2)))
	.subscribe(tlp => { 	
		opts.pub.write(tlp.val2);
		opts.logger.write({oper: "req", status : "success", request : tlp.val1, notif : tlp.val2});
	});		
}

function queryPortf(portfColl: mongo.Collection, portfName: string) : Rx.Observable<any> {
	return portfColl.find({_id : portfName}).query().defaultIfEmpty(null).single();
}
