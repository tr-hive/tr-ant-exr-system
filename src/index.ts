import logs = require("da-logs");

import rabbit = require("da-rabbitmq-rx");
import handler = require("./handler");
import requestsHandler = require("./request-handler");
import mongoRx = require("da-mongo-rx");
 
function getEnvVar(name: string) : string {
	return process.env[name] || 
	process.env["npm_config_" + name] || 
	process.env["npm_package_config_" + name];	
}

const MONGO_URI = getEnvVar("MONGO_URI");
const RABBIT_URI = getEnvVar("RABBIT_URI");
const PORTFOLIO_START_AMOUNT = parseInt(getEnvVar("PORTFOLIO_START_AMOUNT"));
const LOG_LOGGLY_KEY = getEnvVar("LOG_LOGGLY_KEY");
const LOG_LOGGLY_SUBDOMAIN = getEnvVar("LOG_LOGGLY_SUBDOMAIN");
const LOG_MONGO_URI = getEnvVar("LOG_MONGO_URI");
const LOG_MONGO_COLLECTION = getEnvVar("LOG_MONGO_COLLECTION");
const RABBIT_QUEUE_COMMANDS = getEnvVar("RABBIT_QUEUE_COMMANDS");
const RABBIT_QUEUE_QUOTES = getEnvVar("RABBIT_QUEUE_QUOTES");
const RABBIT_QUEUE_NOTIFS = getEnvVar("RABBIT_QUEUE_NOTIFS");
const RABBIT_QUEUE_REQUEST_NOTIF = getEnvVar("RABBIT_QUEUE_REQUEST_NOTIF");
const EXR_SYSTEM_LOGGER_MODE = !!getEnvVar("EXR_SYSTEM_LOGGER_MODE");
  
var logger = new logs.LoggerCompose({pack : <any>require("../package.json"), tags : []},  {
    loggly: {token: LOG_LOGGLY_KEY, subdomain: LOG_LOGGLY_SUBDOMAIN},
    mongo: {connection: LOG_MONGO_URI, collection: LOG_MONGO_COLLECTION},
    console: true
 });
 
logger.write({status : "config",
config : {
MONGO_URI: MONGO_URI,
RABBIT_URI: RABBIT_URI,
PORTFOLIO_START_AMOUNT:PORTFOLIO_START_AMOUNT,
LOG_LOGGLY_KEY:LOG_LOGGLY_KEY,
LOG_LOGGLY_SUBDOMAIN:LOG_LOGGLY_SUBDOMAIN,
LOG_MONGO_URI:LOG_MONGO_URI,
LOG_MONGO_COLLECTION:LOG_MONGO_COLLECTION,
RABBIT_QUEUE_COMMANDS:RABBIT_QUEUE_COMMANDS,
RABBIT_QUEUE_QUOTES:RABBIT_QUEUE_QUOTES,
RABBIT_QUEUE_NOTIFS:RABBIT_QUEUE_NOTIFS,
RABBIT_QUEUE_REQUEST_NOTIF:RABBIT_QUEUE_REQUEST_NOTIF,
EXR_SYSTEM_LOGGER_MODE:EXR_SYSTEM_LOGGER_MODE
}
}); 
 
var commandsSubOpts = {uri: RABBIT_URI, socketType: rabbit.SocketType.SUB, queue: RABBIT_QUEUE_COMMANDS};
var commandsSub = new rabbit.RabbitSub(commandsSubOpts); 
commandsSub.connect();
commandsSub.stream.take(1).subscribe(() =>
 logger.write({resource: "rabbit", oper: "connected", status : "success", opts: commandsSubOpts}) 
, (err) => {
	logger.write({resource: "rabbit", oper: "connected", status : "error", err: err, opts: commandsSubOpts});
	process.exit(1);
});

var quotesSubOpts = {uri: RABBIT_URI, socketType: rabbit.SocketType.SUB, queue: RABBIT_QUEUE_QUOTES};
var quotesSub = new rabbit.RabbitSub(quotesSubOpts); 
quotesSub.connect();
quotesSub.stream.take(1).subscribe(() =>
 logger.write({resource: "rabbit", oper: "connected", status : "success", opts: quotesSubOpts}) 
, (err) => {
	logger.write({resource: "rabbit", oper: "connected", status : "error", err: err, opts: quotesSubOpts});
	process.exit(1);
});

var notifsPubOpts = {uri: RABBIT_URI, socketType: rabbit.SocketType.PUB, queue: RABBIT_QUEUE_NOTIFS};
var notifsPub = new rabbit.RabbitPub(notifsPubOpts); 
notifsPub.connect();
notifsPub.connectStream.subscribe(() =>
 logger.write({resource: "rabbit", oper: "connected", status : "success", opts: notifsPubOpts}) 
, (err) => {
	logger.write({resource: "rabbit", oper: "connected", status : "error", err: err, opts: notifsPubOpts});
	process.exit(1);
});


var requestNotifSubOpts = {uri: RABBIT_URI, socketType: rabbit.SocketType.SUB, queue: RABBIT_QUEUE_REQUEST_NOTIF};
var requestNotifSub = new rabbit.RabbitSub(requestNotifSubOpts); 
requestNotifSub.connect();
requestNotifSub.stream.take(1).subscribe(() =>
 logger.write({resource: "rabbit", oper: "connected", status : "success", opts: requestNotifSubOpts}) 
, (err) => {
	logger.write({resource: "rabbit", oper: "connected", status : "error", err: err, opts: requestNotifSubOpts});
	process.exit(1);
});

var mongo = new mongoRx.MongoDb(MONGO_URI, ["trades", "portfolios", "orders", "lock"]);

var handleOpts : handler.IHandleOpts = {
	commandsStream: commandsSub.stream.skip(1).share(),
	quotesStream: quotesSub.stream.skip(1).share(),			
	tradesColl: mongo.getCollection("trades"),
	portfoliosColl: mongo.getCollection("portfolios"),
	ordersColl: mongo.getCollection("orders"),
	startPortfolioValue: PORTFOLIO_START_AMOUNT,
	logger: logger,
	pub: notifsPub,	
	lock(id: string) {return mongo.lock(id, "lock_exr_system");},
	loggerMode: EXR_SYSTEM_LOGGER_MODE
};

handler.handle(handleOpts);

commandsSub.stream.skip(1).subscribe((val) => { 
}	  
, (err) => {
	logger.write({resource: "rabbit", oper: "runtime", status : "error", err: err, opts: commandsSub});
	process.exit(1);
}, () => {
	logger.write({resource: "rabbit", oper: "runtime", status : "success", opts: commandsSub});
	process.exit(0);	
});

  
quotesSub.stream.skip(1).subscribe((val) => {
}	  
, (err) => {
	logger.write({resource: "rabbit", oper: "runtime", status : "error", err: err, opts: quotesSub});
	process.exit(1);
}, () => {
	logger.write({resource: "rabbit", oper: "runtime", status : "success", opts: quotesSub});
	process.exit(0);	
});

if (!EXR_SYSTEM_LOGGER_MODE) {
	
	var requestsOpts: requestsHandler.IHandleNotifRequestsOpts =  {
		logger: logger,
		pub: notifsPub,
		requestsStream : requestNotifSub.stream.skip(1).share(), 
		portfColl: mongo.getCollection("portfolios")
	}
	
	requestsHandler.handleNotifRequests(requestsOpts);
	
	requestNotifSub.stream.skip(1).subscribe((val) => {
	}	  
	, (err) => {
		logger.write({resource: "rabbit", oper: "runtime", status : "error", err: err, opts: requestNotifSub});
		process.exit(1);
	}, () => {
		logger.write({resource: "rabbit", oper: "runtime", status : "success", opts: requestNotifSub});
		process.exit(0);	
	});
  
}
