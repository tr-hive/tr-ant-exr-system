import mongo = require("da-mongo-rx");
import logs = require("da-logs");
import rabbit = require("da-rabbitmq-rx");
import Rx = require("rx");
import utils = require("tr-ant-utils");
import combinator = require("da-combinator-rx");

 
import tulpe = utils.tulpe;

var uuid = require("node-uuid");

const PKG_NAME = require("../package.json").name;


interface IPosition {
    ticket: string
    quantity: number
    values: number[] 
    date: Date
}

interface IPortfolio {
    _id: string
    value: number
    positions: IPosition[]
    created : Date
}

interface ITrade {
    ticket: string
    portfolio: string
    created : Date
    quantity: number
    price: number
    oper: string
    key: string
    cmd : utils.ICmd
}


export interface IHandleOpts {
    commandsStream: Rx.Observable<utils.ICmd>
    quotesStream: Rx.Observable<utils.IQuote[]>
    logger: logs.ILogger 
    tradesColl: mongo.Collection
    portfoliosColl: mongo.Collection
    ordersColl: mongo.Collection
    startPortfolioValue: number
    pub: rabbit.RabbitPub
    lock(id: string) : Rx.Observable<boolean> 
    loggerMode: boolean 
}

/**
 * Execute command for sell, only if notification for command was emitted by this package.
 * TODO: Should be done more universal. 
 */
function isSellFromSamePackage(command: utils.ICmd) : boolean {
    return command.trade.oper != "sell" || command.notif.issuer == PKG_NAME;                
}

export function handle(opts: IHandleOpts) {
    
    var quotesStream = opts.quotesStream
    //faltten
    .selectMany(x => Rx.Observable.fromArray(x))
    // {SBER: latestPrice, URKA: latestPrice}
    .share();
    
    var commandsStream = opts.commandsStream.share();
    
    var groupedStream = combinator.combineGroup(
        commandsStream.filter(isSellFromSamePackage), 
        quotesStream, 
        (val) =>  val.type == combinator.StreamType.primary ? (<utils.ICmd>val.item).trade.ticket : (<utils.IQuote>val.item).ticket    
    );
    
    groupedStream.subscribe(x =>  {
        write(x.p, x.s, opts)
    });        
}

 
function write(command: utils.ICmd, quote: utils.IQuote, opts: IHandleOpts) {
    
    opts.logger.write({oper: "exe", status: "start", data : { cmd: command, quote : quote} });     
    
    var tradesColl = opts.tradesColl;
    var portfColl = opts.portfoliosColl;
    var ordersColl = opts.ordersColl;
    	        
    var defaultPosition : IPosition = {
        ticket: command.trade.ticket,
        quantity: 0,
        values: [],
        date: new Date()
    };
    var defaultPortfolio : IPortfolio = {
        _id: command.portfolio,
        value: opts.startPortfolioValue,
        created: new Date(),
        positions: []
    };    
        
    var trade : ITrade = {
        ticket: command.trade.ticket,
        portfolio: command.portfolio,
        created : new Date(),
        quantity: command.trade.quantity,
        price: 0, 
        oper: command.trade.oper,
        key: command.key,
        cmd : command
    };

    //mutate this value, in order not to drag between all calls
        
    opts.lock(trade.key)
    .filter(val => val)
    .flatMap(ordersColl.insert(trade))        
    .flatMap(val => {
        //insert trade
        trade.price = quote.latestPrice;                
        return tradesColl.insert(trade);  
    })
    .flatMap(val => {
        
        //create or modify portfolio
        
        var bulk = portfColl.bulk();
        //insert default portfolio
        bulk.insert(defaultPortfolio);
        //insert default ticket position
        bulk.find({_id: command.portfolio, "positions.ticket": {$ne: command.trade.ticket}}).update({$push: {positions: defaultPosition}});                   
        //update ticket position and portfolio params
        bulk.find({_id: command.portfolio, "positions.ticket": command.trade.ticket})
        .update({
                $inc: {
                    "positions.$.quantity": (trade.oper == "buy" ? 1 : -1) * trade.quantity,
                    "value": (trade.oper == "buy" ? -1 : 1) * trade.quantity * trade.price
                },
                $push: {
                    "positions.$.values": {
                        price : trade.price, 
                        quantity : (trade.oper == "buy" ? 1 : -1) * trade.quantity,
                        trade : (<any>trade)._id.toString()
                    }                        
                }
            }              
        );
        //remove positions with 0 quantities
        bulk.find({_id: command.portfolio, "positions.ticket": command.trade.ticket})
        .updateOne({
                $pull: {
                    positions : {quantity: 0 }
                }
         });
        
        return bulk.execute()
        .concat(
            portfColl.find({_id : command.portfolio}).query().last()
        )
        .last();
    })
    .map(portf   => {                     
        return tulpe(portf, mapPortfolio(portf, command));
    })
    .subscribe(val => {
        if (!opts.loggerMode) {
            opts.pub.write(val.val2);
        }
        opts.logger.write({oper: "exe", status: "success", data : { cmd: command, portf: val.val1, notif: val.val2 } }); 
    });	
}

export function mapPortfolio(portf: any, command? : utils.ICmd): utils.INotif<utils.INotifPortfolioChanged> {
    
    var positions = portf.positions ? portf.positions.map((m) => {
        var buys = m.values.filter((f: any) => f.quantity > 0);
        return {
            ticket: m.ticket,
            quantity: m.quantity,
            value: buys.reduce((a: number, b: any) => a + b.price, 0) / buys.length//avg
        }}) : [];        
                                    
    
    var notif : utils.INotif<utils.INotifPortfolioChanged> = {
        key: uuid.v4() + "_" + PKG_NAME,
        type: "INotifPortfolioChanged",
        issuer: PKG_NAME,
        date : (new Date()).toISOString(),                     
        data : {
            portfolio: portf._id,
            value: portf.value,
            cmd: command,
            positions: positions             
        }       
    };           
        
    return notif;    
}