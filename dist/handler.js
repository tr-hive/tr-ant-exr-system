var Rx = require("rx");
var utils = require("tr-ant-utils");
var combinator = require("da-combinator-rx");
var tulpe = utils.tulpe;
var uuid = require("node-uuid");
var PKG_NAME = require("../package.json").name;
/**
 * Execute command for sell, only if notification for command was emitted by this package.
 * TODO: Should be done more universal.
 */
function isSellFromSamePackage(command) {
    return command.trade.oper != "sell" || command.notif.issuer == PKG_NAME;
}
function handle(opts) {
    var quotesStream = opts.quotesStream
        .selectMany(function (x) { return Rx.Observable.fromArray(x); })
        .share();
    var commandsStream = opts.commandsStream.share();
    var groupedStream = combinator.combineGroup(commandsStream.filter(isSellFromSamePackage), quotesStream, function (val) { return val.type == combinator.StreamType.primary ? val.item.trade.ticket : val.item.ticket; });
    groupedStream.subscribe(function (x) {
        write(x.p, x.s, opts);
    });
}
exports.handle = handle;
function write(command, quote, opts) {
    opts.logger.write({ oper: "exe", status: "start", data: { cmd: command, quote: quote } });
    var tradesColl = opts.tradesColl;
    var portfColl = opts.portfoliosColl;
    var ordersColl = opts.ordersColl;
    var defaultPosition = {
        ticket: command.trade.ticket,
        quantity: 0,
        values: [],
        date: new Date()
    };
    var defaultPortfolio = {
        _id: command.portfolio,
        value: opts.startPortfolioValue,
        created: new Date(),
        positions: []
    };
    var trade = {
        ticket: command.trade.ticket,
        portfolio: command.portfolio,
        created: new Date(),
        quantity: command.trade.quantity,
        price: 0,
        oper: command.trade.oper,
        key: command.key,
        cmd: command
    };
    //mutate this value, in order not to drag between all calls
    opts.lock(trade.key)
        .filter(function (val) { return val; })
        .flatMap(ordersColl.insert(trade))
        .flatMap(function (val) {
        //insert trade
        trade.price = quote.latestPrice;
        return tradesColl.insert(trade);
    })
        .flatMap(function (val) {
        //create or modify portfolio
        var bulk = portfColl.bulk();
        //insert default portfolio
        bulk.insert(defaultPortfolio);
        //insert default ticket position
        bulk.find({ _id: command.portfolio, "positions.ticket": { $ne: command.trade.ticket } }).update({ $push: { positions: defaultPosition } });
        //update ticket position and portfolio params
        bulk.find({ _id: command.portfolio, "positions.ticket": command.trade.ticket })
            .update({
            $inc: {
                "positions.$.quantity": (trade.oper == "buy" ? 1 : -1) * trade.quantity,
                "value": (trade.oper == "buy" ? -1 : 1) * trade.quantity * trade.price
            },
            $push: {
                "positions.$.values": {
                    price: trade.price,
                    quantity: (trade.oper == "buy" ? 1 : -1) * trade.quantity,
                    trade: trade._id.toString()
                }
            }
        });
        //remove positions with 0 quantities
        bulk.find({ _id: command.portfolio, "positions.ticket": command.trade.ticket })
            .updateOne({
            $pull: {
                positions: { quantity: 0 }
            }
        });
        return bulk.execute()
            .concat(portfColl.find({ _id: command.portfolio }).query().last())
            .last();
    })
        .map(function (portf) {
        return tulpe(portf, mapPortfolio(portf, command));
    })
        .subscribe(function (val) {
        if (!opts.loggerMode) {
            opts.pub.write(val.val2);
        }
        opts.logger.write({ oper: "exe", status: "success", data: { cmd: command, portf: val.val1, notif: val.val2 } });
    });
}
function mapPortfolio(portf, command) {
    var positions = portf.positions ? portf.positions.map(function (m) {
        var buys = m.values.filter(function (f) { return f.quantity > 0; });
        return {
            ticket: m.ticket,
            quantity: m.quantity,
            value: buys.reduce(function (a, b) { return a + b.price; }, 0) / buys.length //avg
        };
    }) : [];
    var notif = {
        key: uuid.v4() + "_" + PKG_NAME,
        type: "INotifPortfolioChanged",
        issuer: PKG_NAME,
        date: (new Date()).toISOString(),
        data: {
            portfolio: portf._id,
            value: portf.value,
            cmd: command,
            positions: positions
        }
    };
    return notif;
}
exports.mapPortfolio = mapPortfolio;
