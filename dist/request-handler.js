var trAnt = require("tr-ant-utils");
var handler = require("./handler");
var tulpe = trAnt.tulpe;
var PKG_NAME = require("../package.json").name;
function handleNotifRequests(opts) {
    var portfsStream = opts.requestsStream.filter(function (val) { return val.type == "INotifPortfolioChanged"; })
        .do(function (val) { return opts.logger.write({ oper: "req", status: "start", request: val }); })
        .flatMap(function (val) {
        return queryPortf(opts.portfColl, val.query.portfolio).map(function (portf) { return tulpe(val, portf); });
    })
        .do(function (tlp) {
        if (tlp.val2 == null) {
            opts.logger.write({ oper: "req", status: "not_found", request: tlp.val1 });
        }
    })
        .filter(function (tlp) { return tlp.val2; })
        .map(function (tlp) { return tulpe(tlp.val1, handler.mapPortfolio(tlp.val2)); })
        .subscribe(function (tlp) {
        opts.pub.write(tlp.val2);
        opts.logger.write({ oper: "req", status: "success", request: tlp.val1, notif: tlp.val2 });
    });
}
exports.handleNotifRequests = handleNotifRequests;
function queryPortf(portfColl, portfName) {
    return portfColl.find({ _id: portfName }).query().defaultIfEmpty(null).single();
}
