import mongo = require("da-mongo-rx");
import logs = require("da-logs");
import rabbit = require("da-rabbitmq-rx");
import Rx = require("rx");
import utils = require("tr-ant-utils");
export interface IHandleOpts {
    commandsStream: Rx.Observable<utils.ICmd>;
    quotesStream: Rx.Observable<utils.IQuote[]>;
    logger: logs.ILogger;
    tradesColl: mongo.Collection;
    portfoliosColl: mongo.Collection;
    ordersColl: mongo.Collection;
    startPortfolioValue: number;
    pub: rabbit.RabbitPub;
    lock(id: string): Rx.Observable<boolean>;
    loggerMode: boolean;
}
export declare function handle(opts: IHandleOpts): void;
export declare function mapPortfolio(portf: any, command?: utils.ICmd): utils.INotif<utils.INotifPortfolioChanged>;
