import mongo = require("da-mongo-rx");
import rabbit = require("da-rabbitmq-rx");
import logs = require("da-logs");
import Rx = require("rx");
import trAnt = require("tr-ant-utils");
export interface IHandleNotifRequestsOpts {
    logger: logs.ILogger;
    pub: rabbit.RabbitPub;
    requestsStream: Rx.Observable<trAnt.INotifRequest<any>>;
    portfColl: mongo.Collection;
}
export declare function handleNotifRequests(opts: IHandleNotifRequestsOpts): void;
