var logs = require("da-logs");
var rabbit = require("da-rabbitmq-rx");
var handler = require("./handler");
var requestsHandler = require("./request-handler");
var mongoRx = require("da-mongo-rx");
function getEnvVar(name) {
    return process.env[name] ||
        process.env["npm_config_" + name] ||
        process.env["npm_package_config_" + name];
}
var MONGO_URI = getEnvVar("MONGO_URI");
var RABBIT_URI = getEnvVar("RABBIT_URI");
var PORTFOLIO_START_AMOUNT = parseInt(getEnvVar("PORTFOLIO_START_AMOUNT"));
var LOG_LOGGLY_KEY = getEnvVar("LOG_LOGGLY_KEY");
var LOG_LOGGLY_SUBDOMAIN = getEnvVar("LOG_LOGGLY_SUBDOMAIN");
var LOG_MONGO_URI = getEnvVar("LOG_MONGO_URI");
var LOG_MONGO_COLLECTION = getEnvVar("LOG_MONGO_COLLECTION");
var RABBIT_QUEUE_COMMANDS = getEnvVar("RABBIT_QUEUE_COMMANDS");
var RABBIT_QUEUE_QUOTES = getEnvVar("RABBIT_QUEUE_QUOTES");
var RABBIT_QUEUE_NOTIFS = getEnvVar("RABBIT_QUEUE_NOTIFS");
var RABBIT_QUEUE_REQUEST_NOTIF = getEnvVar("RABBIT_QUEUE_REQUEST_NOTIF");
var EXR_SYSTEM_LOGGER_MODE = !!getEnvVar("EXR_SYSTEM_LOGGER_MODE");
var logger = new logs.LoggerCompose({ pack: require("../package.json"), tags: [] }, {
    loggly: { token: LOG_LOGGLY_KEY, subdomain: LOG_LOGGLY_SUBDOMAIN },
    mongo: { connection: LOG_MONGO_URI, collection: LOG_MONGO_COLLECTION },
    console: true
});
logger.write({ status: "config",
    config: {
        MONGO_URI: MONGO_URI,
        RABBIT_URI: RABBIT_URI,
        PORTFOLIO_START_AMOUNT: PORTFOLIO_START_AMOUNT,
        LOG_LOGGLY_KEY: LOG_LOGGLY_KEY,
        LOG_LOGGLY_SUBDOMAIN: LOG_LOGGLY_SUBDOMAIN,
        LOG_MONGO_URI: LOG_MONGO_URI,
        LOG_MONGO_COLLECTION: LOG_MONGO_COLLECTION,
        RABBIT_QUEUE_COMMANDS: RABBIT_QUEUE_COMMANDS,
        RABBIT_QUEUE_QUOTES: RABBIT_QUEUE_QUOTES,
        RABBIT_QUEUE_NOTIFS: RABBIT_QUEUE_NOTIFS,
        RABBIT_QUEUE_REQUEST_NOTIF: RABBIT_QUEUE_REQUEST_NOTIF,
        EXR_SYSTEM_LOGGER_MODE: EXR_SYSTEM_LOGGER_MODE
    }
});
var commandsSubOpts = { uri: RABBIT_URI, socketType: rabbit.SocketType.SUB, queue: RABBIT_QUEUE_COMMANDS };
var commandsSub = new rabbit.RabbitSub(commandsSubOpts);
commandsSub.connect();
commandsSub.stream.take(1).subscribe(function () {
    return logger.write({ resource: "rabbit", oper: "connected", status: "success", opts: commandsSubOpts });
}, function (err) {
    logger.write({ resource: "rabbit", oper: "connected", status: "error", err: err, opts: commandsSubOpts });
    process.exit(1);
});
var quotesSubOpts = { uri: RABBIT_URI, socketType: rabbit.SocketType.SUB, queue: RABBIT_QUEUE_QUOTES };
var quotesSub = new rabbit.RabbitSub(quotesSubOpts);
quotesSub.connect();
quotesSub.stream.take(1).subscribe(function () {
    return logger.write({ resource: "rabbit", oper: "connected", status: "success", opts: quotesSubOpts });
}, function (err) {
    logger.write({ resource: "rabbit", oper: "connected", status: "error", err: err, opts: quotesSubOpts });
    process.exit(1);
});
var notifsPubOpts = { uri: RABBIT_URI, socketType: rabbit.SocketType.PUB, queue: RABBIT_QUEUE_NOTIFS };
var notifsPub = new rabbit.RabbitPub(notifsPubOpts);
notifsPub.connect();
notifsPub.connectStream.subscribe(function () {
    return logger.write({ resource: "rabbit", oper: "connected", status: "success", opts: notifsPubOpts });
}, function (err) {
    logger.write({ resource: "rabbit", oper: "connected", status: "error", err: err, opts: notifsPubOpts });
    process.exit(1);
});
var requestNotifSubOpts = { uri: RABBIT_URI, socketType: rabbit.SocketType.SUB, queue: RABBIT_QUEUE_REQUEST_NOTIF };
var requestNotifSub = new rabbit.RabbitSub(requestNotifSubOpts);
requestNotifSub.connect();
requestNotifSub.stream.take(1).subscribe(function () {
    return logger.write({ resource: "rabbit", oper: "connected", status: "success", opts: requestNotifSubOpts });
}, function (err) {
    logger.write({ resource: "rabbit", oper: "connected", status: "error", err: err, opts: requestNotifSubOpts });
    process.exit(1);
});
var mongo = new mongoRx.MongoDb(MONGO_URI, ["trades", "portfolios", "orders", "lock"]);
var handleOpts = {
    commandsStream: commandsSub.stream.skip(1).share(),
    quotesStream: quotesSub.stream.skip(1).share(),
    tradesColl: mongo.getCollection("trades"),
    portfoliosColl: mongo.getCollection("portfolios"),
    ordersColl: mongo.getCollection("orders"),
    startPortfolioValue: PORTFOLIO_START_AMOUNT,
    logger: logger,
    pub: notifsPub,
    lock: function (id) { return mongo.lock(id, "lock_exr_system"); },
    loggerMode: EXR_SYSTEM_LOGGER_MODE
};
handler.handle(handleOpts);
commandsSub.stream.skip(1).subscribe(function (val) {
}, function (err) {
    logger.write({ resource: "rabbit", oper: "runtime", status: "error", err: err, opts: commandsSub });
    process.exit(1);
}, function () {
    logger.write({ resource: "rabbit", oper: "runtime", status: "success", opts: commandsSub });
    process.exit(0);
});
quotesSub.stream.skip(1).subscribe(function (val) {
}, function (err) {
    logger.write({ resource: "rabbit", oper: "runtime", status: "error", err: err, opts: quotesSub });
    process.exit(1);
}, function () {
    logger.write({ resource: "rabbit", oper: "runtime", status: "success", opts: quotesSub });
    process.exit(0);
});
if (!EXR_SYSTEM_LOGGER_MODE) {
    var requestsOpts = {
        logger: logger,
        pub: notifsPub,
        requestsStream: requestNotifSub.stream.skip(1).share(),
        portfColl: mongo.getCollection("portfolios")
    };
    requestsHandler.handleNotifRequests(requestsOpts);
    requestNotifSub.stream.skip(1).subscribe(function (val) {
    }, function (err) {
        logger.write({ resource: "rabbit", oper: "runtime", status: "error", err: err, opts: requestNotifSub });
        process.exit(1);
    }, function () {
        logger.write({ resource: "rabbit", oper: "runtime", status: "success", opts: requestNotifSub });
        process.exit(0);
    });
}
