/// <reference path="../typings/tsd.d.ts"/>
/// <reference path="../dist/handler.d.ts"/>
import chai = require('chai'); 
import handler = require('../dist/handler');
import rabbit = require('da-rabbitmq-rx');
import mongoRx = require("da-mongo-rx");
import Rx = require("rx");
import trAnt = require("tr-ant-utils");
var expect = chai.expect;

const DATE_REGEX = "\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}\\.\\d{3}Z";

function getEnvVar(name: string) : string {
	return process.env[name] || 
	process.env["npm_config_" + name] || 
	process.env["npm_package_config_" + name];	
}

describe("second test trade",  () => {
	var mongo: mongoRx.MongoDb;
	
	before(done => {
		mongo = new mongoRx.MongoDb(getEnvVar("MONGO_URI_TEST"), ["trades2", "portfolios2", "orders2", "lock2"]);
		done();
	})
	
	beforeEach(done => {
		mongo.getCollection("trades2").remove({})
		.merge(mongo.getCollection("portfolios2").remove({}))
		.merge(mongo.getCollection("orders2").remove({}))
		.merge(mongo.getCollection("lock2").remove({}))
		.subscribeOnCompleted(done);		
	})

	it("2: buy 20 SBER / buy 10 VTBR / sell 10 SBER / check trades and portfolio / check notification",  (done) => {
				
		var pub = new rabbit.RabbitPub({uri: getEnvVar("RABBIT_URI_TEST"), queue: getEnvVar("RABBIT_QUEUE_NOTIFS") + "2", socketType: rabbit.SocketType.PUB});
		pub.connect();
		var sub = new rabbit.RabbitSub({uri: getEnvVar("RABBIT_URI_TEST"), queue: getEnvVar("RABBIT_QUEUE_NOTIFS") + "2", socketType: rabbit.SocketType.SUB});
		sub.connect();
												
		var cmd1 : trAnt.ICmd = {
			key : "1",
			issuer: "tr-ant-cmr-random",
			portfolio: "tr-ant-cmr-random",
			date: (new Date()).toISOString(),
			reason: "test",
			trade : {
				ticket: "SBER", oper : "buy", quantity: 30
			}
		};		
		var quote1 : trAnt.IQuote = {
			ticket: "SBER",
			latestPrice: 100,
			bid: 100,
			ask: 100
		} ;

		var cmd2 : trAnt.ICmd = {
			key : "2",
			reason: "test",
			issuer: "tr-ant-cmr-random",
			portfolio: "tr-ant-cmr-random",
			date: (new Date()).toISOString(),
			trade : {
				ticket: "VTBR", oper : "buy", quantity: 5
			}
		};		
		var quote2 : trAnt.IQuote = {
			ticket: "VTBR",
			latestPrice: 30,
			bid: 30,
			ask: 30
		} ;

		var cmd3 : trAnt.ICmd = {
			key : "3",
			reason: "test",
			issuer: "tr-ant-cmr-random",
			portfolio: "tr-ant-cmr-random",
			date: (new Date()).toISOString(),
			trade : {
				ticket: "SBER", oper : "sell", quantity: 10
			}
		};		
		var quote3 : trAnt.IQuote = {
			ticket: "SBER",
			latestPrice: 110,
			bid: 110,
			ask: 110
		} ;
				
		var commandsStream = Rx.Observable.create<trAnt.ICmd>(observer => {
			setTimeout(() => observer.onNext(cmd1), 10);
			setTimeout(() => observer.onNext(cmd2), 500);			
			setTimeout(() => observer.onNext(cmd3), 1000);
		});
		
		var quotesStream = Rx.Observable.create<trAnt.IQuote[]>(observer => {
			setTimeout(() => observer.onNext([quote1]), 5);
			setTimeout(() => observer.onNext([quote2, quote3]), 450);									
		});
								
		var opts : handler.IHandleOpts = {
			commandsStream: commandsStream,
			quotesStream: quotesStream,			
			tradesColl: mongo.getCollection("trades2"),
			portfoliosColl: mongo.getCollection("portfolios2"),
			ordersColl: mongo.getCollection("orders2"),
			startPortfolioValue: 100000,
			logger: {write: (val) => console.log(val)},
			pub: pub,	
			lock(id: string) {return mongo.lock(id, "lock2");}
		};
		
		sub.stream.take(1).subscribe(_ =>
			handler.handle(opts)
		);
						
		sub.stream.skip(3)
		.do(val => {
			
			console.log(val.data.positions[0]);

			expect(val).has.property("key");
			//expect(val.key).match(new RegExp("^" + DATE_REGEX +  "_tr-ant-exr-system$"));
			expect(val).has.property("issuer", "tr-ant-exr-system");
			expect(val).has.property("date");
			expect(val.date).to.match(new RegExp(DATE_REGEX));
			expect(val).has.property("data");
			expect(val.data).has.property("portfolio", "tr-ant-cmr-random");
			expect(val.data).has.property("value", 97950); // -2000 - 150 + 1100 =  
			expect(val.data).has.property("positions");
			expect(val.data.positions).lengthOf(2);
			expect(val.data.positions[0]).has.property("ticket", "SBER");
			expect(val.data.positions[0]).has.property("quantity", 20); 
			expect(val.data.positions[0]).has.property("value", 100); //only buys						
			expect(val.data.positions[1]).has.property("ticket", "VTBR");
			expect(val.data.positions[1]).has.property("quantity", 5); 
			expect(val.data.positions[1]).has.property("value", 30);
			
			Rx.Observable.zip(
				mongo.getCollection("orders2").find({}).toArray(),
				mongo.getCollection("trades2").find({}).toArray(),
				mongo.getCollection("portfolios2").find({}).toArray()
			, (x, y, z) => [x, y, z])
			.subscribe(val => {
				expect(val[0]).lengthOf(3);
				expect(val[1]).lengthOf(3);
				expect(val[2]).lengthOf(1);							
				
				var expectedOrder1 = {ticket: "SBER", portfolio: "tr-ant-cmr-random", quantity: 30, price: 0, oper: "buy", key: "1"}; 
				var expectedOrder2 = {ticket: "VTBR", portfolio: "tr-ant-cmr-random", quantity: 5, price: 0, oper: "buy", key: "2"};
				var expectedOrder3 = {ticket: "SBER", portfolio: "tr-ant-cmr-random", quantity: 10, price: 0, oper: "sell", key: "3"};

				var expectedTrade1 = {ticket: "SBER", portfolio: "tr-ant-cmr-random", quantity: 30, price: 100, oper: "buy", key: "1"}; 
				var expectedTrade2 = {ticket: "VTBR", portfolio: "tr-ant-cmr-random", quantity: 5, price: 30, oper: "buy", key: "2"};
				var expectedTrade3 = {ticket: "SBER", portfolio: "tr-ant-cmr-random", quantity: 10, price: 110, oper: "sell", key: "3"};
				
				compareTrade(expectedOrder1, val[0][0],  cmd1);
				compareTrade(expectedOrder2, val[0][1], cmd2);
				compareTrade(expectedOrder3, val[0][2], cmd3);

				compareTrade(expectedTrade1, val[1][0], cmd1);
				compareTrade(expectedTrade2, val[1][1], cmd2);
				compareTrade(expectedTrade3, val[1][2], cmd3);
				
				var portf: any = val[2][0];
				
				expect(portf).has.property("_id", "tr-ant-cmr-random");
				expect(portf).has.property("created");
				expect(portf).has.property("positions");
				expect(portf.positions).lengthOf(2);
				expect(portf.positions[0]).has.property("ticket", "SBER");
				expect(portf.positions[0]).has.property("quantity", 20);
				expect(portf.positions[0]).has.property("date");
				expect(portf.positions[0]).has.property("values");
				expect(portf.positions[0].values).lengthOf(2);
				expect(portf.positions[0].values[0]).has.property("trade");
				expect(portf.positions[0].values[0]).has.property("quantity", 30);				
				expect(portf.positions[0].values[0]).has.property("price", 100);
				expect(portf.positions[0].values).lengthOf(2);
				expect(portf.positions[0].values[1]).has.property("trade");
				expect(portf.positions[0].values[1]).has.property("quantity", -10);				
				expect(portf.positions[0].values[1]).has.property("price", 110);

				expect(portf.positions[1]).has.property("ticket", "VTBR");
				expect(portf.positions[1]).has.property("quantity", 5);
				expect(portf.positions[1]).has.property("date");
				expect(portf.positions[1]).has.property("values");
				expect(portf.positions[1].values).lengthOf(1);
				expect(portf.positions[1].values[0]).has.property("trade");
				expect(portf.positions[1].values[0]).has.property("quantity", 5);				
				expect(portf.positions[1].values[0]).has.property("price", 30);


				done();
			});									
		})
		.subscribe(() => {});			
				
	})
	
	function compareTrade(expected: any, actual: any, cmd: trAnt.ICmd) {
			expect(actual).has.property("_id");
			expect(actual).has.property("ticket", expected.ticket);
			expect(actual).has.property("portfolio", expected.portfolio);
			expect(actual).has.property("created");
			expect(actual).has.property("quantity", expected.quantity);
			expect(actual).has.property("price", expected.price);
			expect(actual).has.property("oper", expected.oper);
			expect(actual).has.property("key", expected.key);
			expect(actual).has.property("cmd");
			expect(actual.cmd).eqls(cmd);		
	}
	
});		