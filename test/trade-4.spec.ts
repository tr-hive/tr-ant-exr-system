/// <reference path="../typings/tsd.d.ts"/>
/// <reference path="../dist/handler.d.ts"/>
import chai = require('chai'); 
import handler = require('../dist/handler');
import rabbit = require('da-rabbitmq-rx');
import mongoRx = require("da-mongo-rx");
import Rx = require("rx");
import trAnt = require("tr-ant-utils");
var expect = chai.expect;

const TEST_IDX = 4;
const TRADES_COLL = "trades" + TEST_IDX, PORTFS_COLL = "portfolios" + TEST_IDX, ORDERS_COLL = "orders" + TEST_IDX, LOCK_COLL="lock" + TEST_IDX; 
const RABBIT_QUEUE_NOTIFS = getEnvVar("RABBIT_QUEUE_NOTIFS") + TEST_IDX; 
const DATE_REGEX = "\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}\\.\\d{3}Z";

console.log(getEnvVar("MONGO_URI_TEST"));

function getEnvVar(name: string) : string {
	return process.env[name] || 
	process.env["npm_config_" + name] || 
	process.env["npm_package_config_" + name];	
}

describe("fourth, test trade",  () => {
	var mongo: mongoRx.MongoDb;
	
	before(done => {
		mongo = new mongoRx.MongoDb(getEnvVar("MONGO_URI_TEST"), [TRADES_COLL, PORTFS_COLL, ORDERS_COLL, LOCK_COLL]);
		done();
	})
	
	beforeEach(done => {
		mongo.getCollection(TRADES_COLL).remove({})
		.merge(mongo.getCollection(PORTFS_COLL).remove({}))
		.merge(mongo.getCollection(ORDERS_COLL).remove({}))
		.merge(mongo.getCollection(LOCK_COLL).remove({}))
		.subscribeOnCompleted(done);		
	})

	it("no quote buy 10 SBER / buy 10 SBER / quote arrive / buy SBER 10",  (done) => {
				
		var pub = new rabbit.RabbitPub({uri: getEnvVar("RABBIT_URI_TEST"), queue: RABBIT_QUEUE_NOTIFS, socketType: rabbit.SocketType.PUB});
		pub.connect();
		var sub = new rabbit.RabbitSub({uri: getEnvVar("RABBIT_URI_TEST"), queue: RABBIT_QUEUE_NOTIFS, socketType: rabbit.SocketType.SUB});
		sub.connect();
		
		var quote : trAnt.IQuote = {
			ticket: "SBER",
			latestPrice: 100,
			bid: 100,
			ask: 100
		} ;
												
		var cmd1 : trAnt.ICmd = {
			key : "1",
			issuer: "tr-ant-cmr-random",
			portfolio: "tr-ant-cmr-random",
			reason : "test",
			date: (new Date()).toISOString(),
			trade : {
				ticket: "SBER", oper : "buy", quantity: 10
			}
		};		

		var cmd2 : trAnt.ICmd = {
			key : "2",
			issuer: "tr-ant-cmr-random",
			portfolio: "tr-ant-cmr-random",
			date: (new Date()).toISOString(),
			reason : "test",
			trade : {
				ticket: "SBER", oper : "buy", quantity: 10
			}
		};		

		var cmd3 : trAnt.ICmd = {
			key : "3",
			issuer: "tr-ant-cmr-random",
			portfolio: "tr-ant-cmr-random",
			date: (new Date()).toISOString(),
			reason : "test",
			trade : {
				ticket: "SBER", oper : "buy", quantity: 10
			}
		};		
				
		var commandsStream = Rx.Observable.create<trAnt.ICmd>(observer => {
			setTimeout(() => observer.onNext(cmd1), 100);
			setTimeout(() => observer.onNext(cmd2), 500);			
			setTimeout(() => observer.onNext(cmd3), 1000);
		});
		
		var quotesStream = Rx.Observable.create<trAnt.IQuote[]>(observer => {
			setTimeout(() => observer.onNext([quote]), 750);									
		});
								
		var opts : handler.IHandleOpts = {
			commandsStream: commandsStream,
			quotesStream: quotesStream,			
			tradesColl: mongo.getCollection(TRADES_COLL),
			portfoliosColl: mongo.getCollection(PORTFS_COLL),
			ordersColl: mongo.getCollection(ORDERS_COLL),
			startPortfolioValue: 100000,
			logger: {write: (val) => console.log(val)},
			pub: pub,	
			lock(id: string) {return mongo.lock(id, LOCK_COLL);}
		};
		
		sub.stream.take(1).subscribe(_ =>
			handler.handle(opts)
		);
						
		sub.stream.skip(3)
		.do(val => {
			
			expect(val).has.property("key");
			//expect(val.key).match(new RegExp("^" + DATE_REGEX +  "_tr-ant-exr-system$"));
			expect(val).has.property("issuer", "tr-ant-exr-system");
			expect(val).has.property("date");
			expect(val.date).to.match(new RegExp(DATE_REGEX));
			expect(val).has.property("data");
			expect(val.data).has.property("portfolio", "tr-ant-cmr-random");
			expect(val.data).has.property("value", 97000);  
			expect(val.data).has.property("positions");
			expect(val.data.positions).lengthOf(1);
						
			Rx.Observable.zip(
				mongo.getCollection(ORDERS_COLL).find({}).toArray(),
				mongo.getCollection(TRADES_COLL).find({}).toArray(),
				mongo.getCollection(PORTFS_COLL).find({}).toArray()
			, (x, y, z) => [x, y, z])
			.subscribe(val => {
				expect(val[0]).lengthOf(3);
				expect(val[1]).lengthOf(3);
				expect(val[2]).lengthOf(1);							
				
				var expectedOrder1 = {ticket: "SBER", portfolio: "tr-ant-cmr-random", quantity: 10, price: 0, oper: "buy", key: "1"}; 
				var expectedOrder2 = {ticket: "SBER", portfolio: "tr-ant-cmr-random", quantity: 10, price: 0, oper: "buy", key: "2"};
				var expectedOrder3 = {ticket: "SBER", portfolio: "tr-ant-cmr-random", quantity: 10, price: 0, oper: "buy", key: "3"};

				var expectedTrade1 = {ticket: "SBER", portfolio: "tr-ant-cmr-random", quantity: 10, price: 100, oper: "buy", key: "1"}; 
				var expectedTrade2 = {ticket: "SBER", portfolio: "tr-ant-cmr-random", quantity: 10, price: 100, oper: "buy", key: "2"};
				var expectedTrade3 = {ticket: "SBER", portfolio: "tr-ant-cmr-random", quantity: 10, price: 100, oper: "buy", key: "3"};
				
				compareTrade(expectedOrder1, val[0][0], cmd1);
				compareTrade(expectedOrder2, val[0][1], cmd2);
				compareTrade(expectedOrder3, val[0][2], cmd3);

				compareTrade(expectedTrade1, val[1][0], cmd1);
				compareTrade(expectedTrade2, val[1][1], cmd2);
				compareTrade(expectedTrade3, val[1][2], cmd3);
				
				var portf: any = val[2][0];
				
				expect(portf).has.property("_id", "tr-ant-cmr-random");
				expect(portf).has.property("created");
				expect(portf).has.property("positions");
				expect(portf.positions).lengthOf(1);
				expect(portf.positions[0].values).lengthOf(3);
				
				done();
			});	
											
		})
		.subscribe(() => {});						
	})
	
	function compareTrade(expected: any, actual: any, cmd: trAnt.ICmd) {
			expect(actual).has.property("_id");
			expect(actual).has.property("ticket", expected.ticket);
			expect(actual).has.property("portfolio", expected.portfolio);
			expect(actual).has.property("created");
			expect(actual).has.property("quantity", expected.quantity);
			expect(actual).has.property("price", expected.price);
			expect(actual).has.property("oper", expected.oper);
			expect(actual).has.property("key", expected.key);
			expect(actual).has.property("cmd");
			expect(actual.cmd).eqls(cmd);		
	}
	
});		