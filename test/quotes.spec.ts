/// <reference path="../typings/tsd.d.ts"/>
/// <reference path="../dist/handler.d.ts"/>
import chai = require('chai'); 
import quotes = require('../dist/quotes');
import rabbit = require('da-rabbitmq-rx');
import mongoRx = require("da-mongo-rx");
import trAnt = require("tr-ant-utils");
import Rx = require("rx");
var expect = chai.expect;

function getEnvVar(name: string) : string {
	return process.env[name] || 
	process.env["npm_config_" + name] || 
	process.env["npm_package_config_" + name];	
}

describe("test quotes",  () => {
				
	it("publish quote / check could read latest value",  (done) => {
		var quote : trAnt.IQuote = {
			ticket: "SBER",
			latestPrice: 100,
			bid: 100,
			ask: 100
		} ;						

		var quotesStream = Rx.Observable.create<trAnt.IQuote[]>(observer => {
			observer.onNext([quote])			
		});
		
		quotes.connect(quotesStream);
		
		quotes.getLatestQuote("SBER").subscribe(val => {
			console.log(val);
			expect(val).eqls(quote);
			done();
		});
				
	})	

	it("publish many quotes diffrent tickets / check could read latest value",  (done) => {
		var quote1 : trAnt.IQuote = {
			ticket: "SBER",
			latestPrice: 100,
			bid: 100,
			ask: 100
		} ;
		var quote2 : trAnt.IQuote = {
			ticket: "VTBR",
			latestPrice: 150,
			bid: 150,
			ask: 150
		} ;														
		var quote3 : trAnt.IQuote = {
			ticket: "SBER",
			latestPrice: 200,
			bid: 200,
			ask: 200
		} ;						
		var quote4 : trAnt.IQuote = {
			ticket: "SBER",
			latestPrice: 50,
			bid: 50,
			ask: 50
		} ;						

		var quotesStream = Rx.Observable.create<trAnt.IQuote[]>(observer => {
			observer.onNext([quote1, quote2, quote3, quote4])						
		});
				
		quotes.connect(quotesStream);
		
		Rx.Observable.zip(
			quotes.getLatestQuote("SBER"), 
			quotes.getLatestQuote("VTBR"), (v1, v2) => [v1, v2])
		.subscribe(val => {
			expect(val[0]).eqls(quote4);
			expect(val[1]).eqls(quote2);
			done();
		});
		 				
	});
	
	
	it("subscribe to stream with no quote and ten publish quote",  (done) => {
		var quote1 : trAnt.IQuote = {
			ticket: "SBER",
			latestPrice: 100,
			bid: 100,
			ask: 100
		} ;
		
		var quotesStream = Rx.Observable.create<trAnt.IQuote[]>(observer => {			
			setTimeout(() => observer.onNext([quote1]), 500);			
		});
				
		quotes.connect(quotesStream);
		
		quotes.getLatestQuote("SBER").subscribe(val => {
			expect(val).eqls(quote1);			
			done();
		});
	});
	
	
	it("publish many quotes / check could read latest value",  (done) => {
		var quote1 : trAnt.IQuote = {
			ticket: "SBER",
			latestPrice: 100,
			bid: 100,
			ask: 100
		} ;						
		var quote2 : trAnt.IQuote = {
			ticket: "SBER",
			latestPrice: 200,
			bid: 200,
			ask: 200
		} ;						
		var quote3 : trAnt.IQuote = {
			ticket: "SBER",
			latestPrice: 50,
			bid: 50,
			ask: 50
		} ;						

		var quotesStream = Rx.Observable.create<trAnt.IQuote[]>(observer => {
			observer.onNext([quote1, quote2, quote3]);						
		});
				
		quotes.connect(quotesStream);
				
		quotes.getLatestQuote("SBER").subscribe(val => {
			expect(val).eqls(quote3);
			}, null, done);
	});
	
    it("publish SBER / delay - SBER / delay - check latest SBER", (done) => {
        
		var quote1 = {
            ticket: "SBER",
            latestPrice: 100,
            bid: 100,
            ask: 100
        };
        var quote2 = {
            ticket: "SBER",
            latestPrice: 110,
            bid: 110,
            ask: 110
        };
        
		var quotesStream = Rx.Observable.create<trAnt.IQuote[]>(observer => {			
			observer.onNext([quote1]);
			setTimeout(() => { 
				observer.onNext([quote2]) 
			}, 10);			
		});
		
		var res = quotes.connect(quotesStream);
				
		setTimeout(() => {
			quotes.getLatestQuote("SBER").subscribe(val => {				
				expect(val).eqls(quote2);
				done();
			})}, 
		100);
	
    });
		
});		