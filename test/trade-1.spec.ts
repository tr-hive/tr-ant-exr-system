/// <reference path="../typings/tsd.d.ts"/>
/// <reference path="../dist/handler.d.ts"/>
import chai = require('chai'); 
import handler = require('../dist/handler');
import rabbit = require('da-rabbitmq-rx');
import mongoRx = require("da-mongo-rx");
import Rx = require("rx");
import trAnt = require("tr-ant-utils");
var expect = chai.expect;

const DATE_REGEX = "\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}\\.\\d{3}Z";

function getEnvVar(name: string) : string {
	return process.env[name] || 
	process.env["npm_config_" + name] || 
	process.env["npm_package_config_" + name];	
}

describe("first test trade",  () => {
	var mongo: mongoRx.MongoDb;
	
	before(done => {
		mongo = new mongoRx.MongoDb(getEnvVar("MONGO_URI_TEST"), ["trades1", "portfolios1", "orders1", "lock1"]);
		done();
	})
	
	beforeEach(done => {
		mongo.getCollection("trades1").remove({})
		.merge(mongo.getCollection("portfolios1").remove({}))
		.merge(mongo.getCollection("orders1").remove({}))
		.merge(mongo.getCollection("lock1").remove({}))
		.subscribeOnCompleted(done);		
	})

	it("1: buy 10 SBER / check trades and portfolio / check notification",  (done) => {
				
		var pub = new rabbit.RabbitPub({uri: getEnvVar("RABBIT_URI_TEST"), queue: getEnvVar("RABBIT_QUEUE_NOTIFS") + "1", socketType: rabbit.SocketType.PUB});
		pub.connect();
		var sub = new rabbit.RabbitSub({uri: getEnvVar("RABBIT_URI_TEST"), queue: getEnvVar("RABBIT_QUEUE_NOTIFS") + "1", socketType: rabbit.SocketType.SUB});
		sub.connect();
												
		var cmd : trAnt.ICmd = {
			key : "1",
			issuer: "tr-ant-cmr-random",
			portfolio: "tr-ant-cmr-random",
			date: (new Date()).toISOString(),
			reason: "test",
			trade : {
				ticket: "SBER", oper : "buy", quantity: 10
			}
		};
		
		var quote : trAnt.IQuote = {
			ticket: "SBER",
			latestPrice: 100,
			bid: 100,
			ask: 100
		} ;
				
		var commandsStream = Rx.Observable.create<trAnt.ICmd>(observer => {
		   setTimeout(() => observer.onNext(cmd), 10);			
		});
		
		var quotesStream = Rx.Observable.create<trAnt.IQuote[]>(observer => {
		 	setTimeout(() => observer.onNext([quote]), 20);			
		});
										
		var opts : handler.IHandleOpts = {
			commandsStream: commandsStream,
			quotesStream: quotesStream,			
			tradesColl: mongo.getCollection("trades1"),
			portfoliosColl: mongo.getCollection("portfolios1"),
			ordersColl: mongo.getCollection("orders1"),
			startPortfolioValue: 100000,
			logger: {write: (val) => console.log(val)},
			pub: pub,	
			lock(id: string) {return mongo.lock(id, "lock1");}
		};
		
		sub.stream.take(1).subscribe(_ =>
			handler.handle(opts)
		);
						
		sub.stream.skip(1).subscribe(val => {

			console.log(val.data);
			
			expect(val).has.property("key");
			//expect(val.key).match(new RegExp("^" + DATE_REGEX +  "_tr-ant-exr-system$"));
			expect(val).has.property("issuer", "tr-ant-exr-system");
			expect(val).has.property("date");
			expect(val.date).to.match(new RegExp(DATE_REGEX));
			expect(val).has.property("data");
			expect(val.data).has.property("portfolio", "tr-ant-cmr-random");
			expect(val.data).has.property("value", 99000);
			expect(val.data).has.property("positions");
			expect(val.data.positions).lengthOf(1);
			expect(val.data.positions[0]).has.property("ticket", "SBER");
			expect(val.data.positions[0]).has.property("quantity", 10);
			expect(val.data.positions[0]).has.property("value", 100);
			
			Rx.Observable.zip(
				mongo.getCollection("orders1").find({}).toArray(),
				mongo.getCollection("trades1").find({}).toArray(),
				mongo.getCollection("portfolios1").find({}).toArray()
			, (x, y, z) => [x, y, z])
			.subscribe(val => {
				expect(val[0]).lengthOf(1);
				expect(val[1]).lengthOf(1);
				expect(val[2]).lengthOf(1);
				var order: any = val[0][0];
				var trade: any = val[1][0];
				var portf: any = val[2][0];
				expect(order).has.property("_id");
				expect(order).has.property("ticket", "SBER");
				expect(order).has.property("portfolio", "tr-ant-cmr-random");
				expect(order).has.property("created");
				expect(order).has.property("quantity", 10);
				expect(order).has.property("price", 0);
				expect(order).has.property("oper", "buy");
				expect(order).has.property("key", "1");
				expect(order).has.property("cmd");
				expect(order.cmd).eqls(cmd);
				
				expect(trade).has.property("_id");
				expect(trade).has.property("ticket", "SBER");
				expect(trade).has.property("portfolio", "tr-ant-cmr-random");
				expect(trade).has.property("created");
				expect(trade).has.property("quantity", 10);
				expect(trade).has.property("price", 100);
				expect(trade).has.property("oper", "buy");
				expect(trade).has.property("key", "1");
				expect(trade).has.property("cmd");
				expect(trade.cmd).eqls(cmd);
				

				expect(portf).has.property("_id", "tr-ant-cmr-random");
				expect(portf).has.property("created");
				expect(portf).has.property("positions");
				expect(portf.positions).lengthOf(1);
				expect(portf.positions[0]).has.property("ticket", "SBER");
				expect(portf.positions[0]).has.property("quantity", 10);
				expect(portf.positions[0]).has.property("date");
				expect(portf.positions[0]).has.property("values");
				expect(portf.positions[0].values).lengthOf(1);
				expect(portf.positions[0].values[0]).has.property("trade");
				expect(portf.positions[0].values[0]).has.property("quantity", 10);				
				expect(portf.positions[0].values[0]).has.property("price", 100);
														
				done();
			});
			
			
		});			
				
	})
	
});		
