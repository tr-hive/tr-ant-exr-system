/// <reference path="../typings/tsd.d.ts"/>
/// <reference path="../dist/handler.d.ts"/>
import chai = require('chai'); 
import handler = require('../dist/handler');
import reqHandler = require('../dist/request-handler');
import rabbit = require('da-rabbitmq-rx');
import mongoRx = require("da-mongo-rx");
import Rx = require("rx");
import trAnt = require("tr-ant-utils");
var expect = chai.expect;

const TEST_IDX = 5;
const TRADES_COLL = "trades" + TEST_IDX, PORTFS_COLL = "portfolios" + TEST_IDX, ORDERS_COLL = "orders" + TEST_IDX, LOCK_COLL="lock" + TEST_IDX; 
const RABBIT_QUEUE_NOTIFS = getEnvVar("RABBIT_QUEUE_NOTIFS") + TEST_IDX; 


console.log(getEnvVar("MONGO_URI_TEST"));

const MONGO_URI_TEST = getEnvVar("MONGO_URI_TEST");
const RABBIT_URI_TEST = getEnvVar("RABBIT_URI_TEST");

function getEnvVar(name: string) : string {
	return process.env[name] || 
	process.env["npm_config_" + name] || 
	process.env["npm_package_config_" + name];	
}

var expectedNotif = 
{"type":"INotifPortfolioChanged","issuer":"tr-ant-exr-system","data":{"portfolio":"test","value":99000,"positions":[{"ticket":"SBER","quantity":10,"value":100}]}};


describe("make trades, request portfolio",  () => {
	var mongo: mongoRx.MongoDb;
	
	before(done => {
		mongo = new mongoRx.MongoDb(MONGO_URI_TEST, [TRADES_COLL, PORTFS_COLL, ORDERS_COLL, LOCK_COLL]);
		done();
	})
	
	beforeEach(done => {
		mongo.getCollection(TRADES_COLL).remove({})
		.merge(mongo.getCollection(PORTFS_COLL).remove({}))
		.merge(mongo.getCollection(ORDERS_COLL).remove({}))
		.merge(mongo.getCollection(LOCK_COLL).remove({}))
		.subscribeOnCompleted(done);		
	})

	it("make some trades / request portfolio",  (done) => {
				
		var pub = new rabbit.RabbitPub({uri: RABBIT_URI_TEST, queue: RABBIT_QUEUE_NOTIFS, socketType: rabbit.SocketType.PUB});
		pub.connect();
		var sub = new rabbit.RabbitSub({uri: RABBIT_URI_TEST, queue: RABBIT_QUEUE_NOTIFS, socketType: rabbit.SocketType.SUB});
		sub.connect();		

		
		var quote : trAnt.IQuote = {
			ticket: "SBER",
			latestPrice: 100,
			bid: 100,
			ask: 100
		} ;
												
		var cmd1 : trAnt.ICmd = {
			key : "1",
			issuer: "tr-ant-cmr-random",
			portfolio: "test",
			reason : "test",
			date: (new Date()).toISOString(),
			trade : {
				ticket: "SBER", oper : "buy", quantity: 10
			}
		};		

		var commandsStream = Rx.Observable.create<trAnt.ICmd>(observer => {
			setTimeout(() => observer.onNext(cmd1), 100);
		});
		
		var quotesStream = Rx.Observable.create<trAnt.IQuote[]>(observer => {
			setTimeout(() => observer.onNext([quote]), 200);									
		});
		
		var reqNotifsStream = Rx.Observable.create<trAnt.INotifRequest<trAnt.INotifRequestPortfolioQuery>>(observer => {
			setTimeout(() => observer.onNext({type: "INotifPortfolioChanged", query : {portfolio : "test"}}), 300);
		});		
								
		var opts : handler.IHandleOpts = {
			commandsStream: commandsStream.share(),
			quotesStream: quotesStream.share(),			
			tradesColl: mongo.getCollection(TRADES_COLL),
			portfoliosColl: mongo.getCollection(PORTFS_COLL),
			ordersColl: mongo.getCollection(ORDERS_COLL),
			startPortfolioValue: 100000,
			logger: {write: (val) => console.log(JSON.stringify(val, null, 2))},
			pub: pub,	
			lock(id: string) {return mongo.lock(id, LOCK_COLL);}
		};
		
		handler.handle(opts);
		
		var reqOpts : reqHandler.IHandleNotifRequestsOpts = {
			logger: {write: (val) => console.log(JSON.stringify(val, null, 2))},
			pub: pub,
			requestsStream: reqNotifsStream,
			portfColl: mongo.getCollection(PORTFS_COLL)			
		};
				
		reqHandler.handleNotifRequests(reqOpts);		
		
		sub.stream.skip(2).take(1).subscribe(val => {
			expect(val).have.property("date");
			expect(val).have.property("key");
			delete val.date;
			delete val.key;
			expect(val).eql(expectedNotif);
			done();
			//console.log(JSON.stringify(val, null, 2));
		});
	})
									
});		