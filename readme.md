# Trader micro service

#Description

+ Get commander messages
+ Fill trades table
+ Fill portfolio table
+ Fill orders table

Write to mongodb orders, trades and portfolios for commands.

```

[IQuote]---[SBER-100]--[VTBR-050]--[SBER-101]-------------------------------------[LKOH-50]

[ICmd  ]----------------------------------------[SBER-BUY]----------[LKOH-SELL]--------------


[Do    ]----------------------------------------[BUY-SBER-101]--------------------[SELL-LKOH-50]  
```

## Project structure

+ /src - sources, type script  
  + index.ts - entry point of application
    
+ Dockerfile - docker image builder
+ .envs - define you config variables here (ignored in git)
+ typings - typescript definition files    
  
## Prerequisites

+ nodejs
+ npm
+ typescript


## Install

```
git clone https://bitbucket.org/tr-hive/tr-ant-exr-system
npm install
```  
+ To rebuild `tsc` or `npm run-script build`
+ To watch and rebuild `tsc -w`

Rebuild also executed before each `npm start` 

## Start

+ Define enviroment variables, in order of priority `env`, `.npmrc`, `package.json`
+ Test start `npm start` 

## Docker 

+ Build container `docker build -t baio/tr-ant-exr-system .`
+ Start container `docker run baio/tr-ant-exr-system`

Also build `npm run-script docker` or `npm run-script docker-clean`  